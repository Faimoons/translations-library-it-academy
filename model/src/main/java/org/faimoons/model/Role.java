package org.faimoons.model;


import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;
@Component
@Entity
@Table
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long idRole;
    @Column
    private String name;
    @OneToMany(mappedBy = "role")
    private List<Contact> contacts;

    public Role() {
    }

    public Role(Long idRole, String name) {
        this.idRole = idRole;
        this.name = name;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public Long getIdRole() {
        return idRole;
    }

    public void setIdRole(Long idRole) {
        this.idRole = idRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
