package org.faimoons.model;


import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name = "description_books")
@Component
public class DescriptionBooks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long idBook;
    @Column
    private LocalDate createDate;
    @Column
    private String nameBook;
    @Column
    private String description;
    @Column
    private String image;
    @OneToOne(mappedBy = "descriptionBooks",fetch = FetchType.LAZY,cascade = CascadeType.REFRESH)
    private Books books;
    @ManyToMany(mappedBy = "descriptionBooks", cascade = CascadeType.ALL , fetch = FetchType.LAZY)
    private List<Contact> contacts = new ArrayList<>();

//    @Override
//    public int hashCode() {
//        int result = super.hashCode();
//        result = 31 * result + idBook.hashCode();
//        result = 31 * result + nameBook.hashCode();
//        return result;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DescriptionBooks)) return false;
        DescriptionBooks flight = (DescriptionBooks) o;
        return idBook.equals(flight.idBook) && this.nameBook.equals(flight.nameBook);
    }

    public DescriptionBooks() {
    }

    public DescriptionBooks(Long idBook, String nameBook, String description) {
        this.idBook = idBook;
        this.nameBook = nameBook;
        this.description = description;
    }

    public Books getBooks() {
        return books;
    }

    public List<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

    public void setBooks(Books books) {
        this.books = books;
    }

    public Long getIdBook() {
        return idBook;
    }

    public void setIdBook(Long idBook) {
        this.idBook = idBook;
    }

    public String getNameBook() {
        return nameBook;
    }

    public void setNameBook(String nameBook) {
        this.nameBook = nameBook;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public LocalDate getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }
}
