package org.faimoons.model;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
@Table(name = "books")
@Component
public class Books {
    @Id
    @Column
    private Long idBook;

    @Column
    private String author;

    @Column
    private LocalDate dateWriting;

    @Column
    private String genre;

    @Column
    private String country;

    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name="idBook")
    private DescriptionBooks descriptionBooks;

    @OneToMany(mappedBy = "books",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<ChapterBooks> chapterBooks;

    @OneToMany(mappedBy = "booksChat",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Chat> chat;

    public Books() {
    }

    public Books(Long idBook, String author, LocalDate dateWriting, String genre, String country, DescriptionBooks descriptionBooks, List<ChapterBooks> chapterBooks, List<Chat> chat) {
        this.idBook = idBook;
        this.author = author;
        this.dateWriting = dateWriting;
        this.genre = genre;
        this.country = country;
        this.descriptionBooks = descriptionBooks;
        this.chapterBooks = chapterBooks;
        this.chat = chat;
    }

    public Books(Long idBook, String author, LocalDate createDate, String genre, String country) {
        this.idBook = idBook;
        this.author = author;
        this.dateWriting = createDate;
        this.genre = genre;
        this.country = country;
    }

    public List<Chat> getChat() {
        return chat;
    }

    public void setChat(List<Chat> chat) {
        this.chat = chat;
    }

    public DescriptionBooks getDescriptionBooks() {
        return descriptionBooks;
    }

    public void setDescriptionBooks(DescriptionBooks descriptionBooks) {
        this.descriptionBooks = descriptionBooks;
    }

    public List<ChapterBooks> getChapterBooks() {
        return chapterBooks;
    }

    public void setChapterBooks(List<ChapterBooks> chapterBooks) {
        this.chapterBooks = chapterBooks;
    }

    public Long getIdBook() {
        return idBook;
    }

    public void setIdBook(Long idBook) {
        this.idBook = idBook;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public LocalDate getDateWriting() {
        return dateWriting;
    }

    public void setDateWriting(LocalDate dateWriting) {
        this.dateWriting = dateWriting;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
