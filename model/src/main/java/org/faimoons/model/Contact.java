package org.faimoons.model;


import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "contact")
@Component
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long idContact;

    @Column
    private String email;

    @Column
    private String login;

    @Column
    private String password;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @ManyToOne(cascade = CascadeType.REFRESH , fetch = FetchType.EAGER)
    @JoinColumn(name = "[idRole]")
    private Role role;

    @ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.EAGER)
    @JoinTable(name = "books_to_contact",
            joinColumns = @JoinColumn(name = "idContact") ,
            inverseJoinColumns =  @JoinColumn(name = "idBook"))
    private List<DescriptionBooks> descriptionBooks = new ArrayList<>();

    @OneToMany(mappedBy = "contact",fetch = FetchType.LAZY)
    private List<Chat> chat;

    public Contact() {
    }

    public Contact(Long idContact, String email, String login, String password, String firstName, String lastName, Role role) {
        this.idContact = idContact;
        this.email = email;
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    public List<Chat> getChat() {
        return chat;
    }

    public void setChat(List<Chat> chat) {
        this.chat = chat;
    }

    public List<DescriptionBooks> getDescriptionBooks() {
        return descriptionBooks;
    }

    public void setDescriptionBooks(List<DescriptionBooks> descriptionBooks) {
        this.descriptionBooks = descriptionBooks;
    }
    public void addDescriptionBooks(DescriptionBooks descriptionBooks){
        this.descriptionBooks.add(descriptionBooks);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Long getIdContact() {
        return idContact;
    }

    public void setIdContact(Long idContact) {
        this.idContact = idContact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
