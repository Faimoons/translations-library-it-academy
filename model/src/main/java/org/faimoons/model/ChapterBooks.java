package org.faimoons.model;


import org.springframework.stereotype.Component;

import javax.persistence.*;


@Entity
@Table(name = "chapter_books")
@Component
public class ChapterBooks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idChapter;

    @Column(insertable = false,updatable = false)
    private Long idBook;

    @Column
    private String Chapter;

    @Column
    private String nameChapter;

    @Column
    private int numberChapter;

    @ManyToOne(cascade = CascadeType.REFRESH , fetch = FetchType.LAZY)
    @JoinColumn(name = "idBook")
    private Books books;



    public ChapterBooks() {
    }

    public ChapterBooks(Long idChapter, String chapter, String nameChapter, int numberChapter) {
        this.idChapter = idChapter;
        Chapter = chapter;
        this.nameChapter = nameChapter;
        this.numberChapter = numberChapter;
    }



    public Books getBooks() {
        return books;
    }

    public void setBooks(Books books) {
        this.books = books;
    }

    public String getNameChapter() {
        return nameChapter;
    }

    public void setNameChapter(String nameChapter) {
        this.nameChapter = nameChapter;
    }

    public int getNumberChapter() {
        return numberChapter;
    }

    public void setNumberChapter(int numberChapter) {
        this.numberChapter = numberChapter;
    }

    public Long getIdChapter() {
        return idChapter;
    }

    public void setIdChapter(Long idChapter) {
        this.idChapter = idChapter;
    }

    public Long getIdBook() {
        return idBook;
    }

    public void setIdBook(Long idBook) {
        this.idBook = idBook;
    }

    public String getChapter() {
        return Chapter;
    }

    public void setChapter(String chapter) {
        Chapter = chapter;
    }
}
