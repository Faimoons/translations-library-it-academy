package org.faimoons.model;

public class BooksToContact {
    private Long igContact;
    private Long idBook;

    public BooksToContact() {
    }

    public BooksToContact(Long igContact, Long idBook) {
        this.igContact = igContact;
        this.idBook = idBook;
    }

    public Long getIgContact() {
        return igContact;
    }

    public void setIgContact(Long igContact) {
        this.igContact = igContact;
    }

    public Long getIdBook() {
        return idBook;
    }

    public void setIdBook(Long idBook) {
        this.idBook = idBook;
    }
}
