package org.faimoons.model;



import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDateTime;

@Component
@Entity
@Table
public class Chat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long idChat;

    @Column
    private String nameUser;

    @Column(insertable = false,updatable = false)
    private Long idBook;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "idContact")
    private Contact contact;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "idBook")
    private Books booksChat;

    @Column
    private String message;

    @Column
    private LocalDateTime timeMessage;

    public Chat() {
    }


    public Long getIdChat() {
        return idChat;
    }

    public void setIdChat(Long idChat) {
        this.idChat = idChat;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Books getBooksChat() {
        return booksChat;
    }

    public void setBooksChat(Books booksChat) {
        this.booksChat = booksChat;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTimeMessage() {
        return timeMessage;
    }

    public Long getIdBook() {
        return idBook;
    }

    public void setIdBook(Long idBook) {
        this.idBook = idBook;
    }

    public void setTimeMessage(LocalDateTime timeMessage) {
        this.timeMessage = timeMessage;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

}
