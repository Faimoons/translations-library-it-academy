package org.faimoons.controller;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.commons.io.IOUtils;
import org.faimoons.model.Books;
import org.faimoons.model.DescriptionBooks;
import org.faimoons.services.interfaceServices.BookServicesRepository;
import org.faimoons.services.interfaceServices.ChapterServicesRepository;
import org.faimoons.services.interfaceServices.ChatServiceRepository;
import org.faimoons.services.interfaceServices.DescriptionBooksServicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.annotation.MultipartConfig;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@MultipartConfig
@Controller

public class BookController {

    @Autowired
    private BookServicesRepository bookServicesRepository;

    @Autowired
    private DescriptionBooksServicesRepository descriptionBooksServicesRepository;

    @Autowired
    private ChatServiceRepository chatServicesRepository;

    @Autowired
    private ChapterServicesRepository chapterServicesRepository;

    @GetMapping("/home/book/{idBook}")
    public ModelAndView viewBook(ModelAndView modelAndView,
                                 @PathVariable Long idBook) throws Exception {
        List<Integer> numberChapter = chapterServicesRepository.getAllNumberChapterForBook(idBook);
        Books books = bookServicesRepository.findById(idBook).get();
        modelAndView.addObject("book", books);
        modelAndView.addObject("newBooks", descriptionBooksServicesRepository.findAllByCreateDateBetween());
        modelAndView.addObject("message", chatServicesRepository.findAllByIdBookOrderByTimeMessageDesc(books.getIdBook()));
        if(numberChapter.isEmpty()){
            modelAndView.addObject("countChapter", null);
        }else {
            modelAndView.addObject("countChapter", chapterServicesRepository.getAllNumberChapterForBook(idBook));
        }
        modelAndView.setViewName("book");
        return modelAndView;
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("/home/create/book")
    public ModelAndView viewAddBook(ModelAndView modelAndView) throws Exception {
        modelAndView.addObject("newBooks", descriptionBooksServicesRepository.findAllByCreateDateBetween());
        modelAndView.setViewName("createBook");
        return modelAndView;
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @PostMapping("/home/create/book")
    public String addBook(
            @RequestParam(value = "file") MultipartFile file,
            ModelAndView modelAndView,
            @RequestParam String name,
            @RequestParam String descriptions,
            @RequestParam String author,
            @RequestParam String country,
            @RequestParam String genre,
            @RequestParam String date
    ) throws IOException {
        DescriptionBooks descriptionBooks = new DescriptionBooks();
        modelAndView.setViewName("home");
        descriptionBooks.setNameBook(name);
        descriptionBooks.setDescription(descriptions);
        descriptionBooks.setImage(Base64.encode(IOUtils.toByteArray(file.getInputStream())));
        descriptionBooksServicesRepository.save(descriptionBooks);
        Books books = new Books();
        books.setIdBook(descriptionBooks.getIdBook());
        books.setAuthor(author);
        books.setCountry(country);
        books.setGenre(genre);
        books.setDateWriting(LocalDate.parse(date));
        bookServicesRepository.save(books);
        return "redirect:/home?page=0&size=5";
    }

    @GetMapping("/delete/book/{idBook}")
    public RedirectView deleteBook(@PathVariable Long idBook){
        bookServicesRepository.deleteById(idBook);
        return new RedirectView("/home?page=0&size=5");
    }

}
