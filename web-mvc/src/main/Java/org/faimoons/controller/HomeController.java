package org.faimoons.controller;

import org.faimoons.model.DescriptionBooks;
import org.faimoons.services.interfaceServices.DescriptionBooksServicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping(path = "/home")
public class HomeController {

    @Autowired
    private DescriptionBooksServicesRepository descriptionBooksServicesRepository;

//    @GetMapping("/home")
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView mainPage(ModelAndView modelAndView,
                                 @RequestParam int page,
                                 @RequestParam int size
    ){
        Pageable pageable = PageRequest.of(page, size);
        Page<DescriptionBooks> descriptionBooks = descriptionBooksServicesRepository.findAll(pageable);
        List<Integer> totalPage = new ArrayList<>();
        for (int i = 0; i <= descriptionBooks.getTotalPages()-1; i++) {
            totalPage.add(i);
        }
        modelAndView.addObject("numberPage", page);
        modelAndView.addObject("totalPage",totalPage);
        modelAndView.addObject("books", descriptionBooks.getContent());
        Collection<DescriptionBooks> descriptionBooks1 = descriptionBooksServicesRepository.findAllByCreateDateBetween();
        modelAndView.addObject("newBooks", descriptionBooks1);
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView searchBook(ModelAndView modelAndView,
                                   @RequestParam String search){
        modelAndView.addObject("books",descriptionBooksServicesRepository.findAllByNameBookContaining(search));
        Collection<DescriptionBooks> descriptionBooks1 = descriptionBooksServicesRepository.findAllByCreateDateBetween();
        modelAndView.addObject("newBooks", descriptionBooks1);
        modelAndView.addObject("noPageable", "noPageable");
        modelAndView.setViewName("home");
        return modelAndView;
    }

}
