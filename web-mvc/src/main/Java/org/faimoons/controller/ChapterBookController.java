package org.faimoons.controller;

import org.faimoons.model.Books;
import org.faimoons.model.ChapterBooks;
import org.faimoons.model.DescriptionBooks;
import org.faimoons.services.interfaceServices.BookServicesRepository;
import org.faimoons.services.interfaceServices.ChapterServicesRepository;
import org.faimoons.services.interfaceServices.DescriptionBooksServicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Collection;

@Controller
public class ChapterBookController {

    @Autowired
    private DescriptionBooksServicesRepository descriptionBooksServicesRepository;
    @Autowired
    private ChapterServicesRepository chapterServicesRepository;
    @Autowired
    private BookServicesRepository bookServicesRepository;

    @GetMapping("/home/book/chapter/edit/{idChapter}")
    public ModelAndView viewEditChapterPage(ModelAndView modelAndView,
                                            @PathVariable Long idChapter) {
        ChapterBooks chapterBooks = chapterServicesRepository.findById(idChapter).get();
        modelAndView.addObject("chapter", chapterBooks);
        modelAndView.setViewName("editChapter");
        return modelAndView;
    }

    @PostMapping("/home/book/chapter/edit/{idChapter}")
    public RedirectView editChapterPage(@PathVariable Long idChapter,
                                        @RequestParam String nameChapter,
                                        @RequestParam Integer numberChapter,
                                        @RequestParam String chapter){
        ChapterBooks chapterBooks = chapterServicesRepository.findById(idChapter).get();
        chapterBooks.setNumberChapter(numberChapter);
        chapterBooks.setNameChapter(nameChapter);
        chapterBooks.setChapter(chapter);
        chapterServicesRepository.save(chapterBooks);
        return new RedirectView("/home?page=0&size=5");
    }

    @GetMapping("/home/book/{idBook}/create/chapter")
    public ModelAndView viewCreateChapterPage(ModelAndView modelAndView,
                                              @PathVariable Long idBook) {
        modelAndView.addObject("idBook",idBook);
        modelAndView.setViewName("createChapter");
        return modelAndView;
    }

    @PostMapping("/home/book/{idBook}/create/chapter")
    public RedirectView createChapterBook(@PathVariable Long idBook,
                                          @RequestParam String nameChapter,
                                          @RequestParam Integer numberChapter,
                                          @RequestParam String chapter) {
        ChapterBooks chapterBooks = new ChapterBooks();
        chapterBooks.setChapter(chapter);
        chapterBooks.setNameChapter(nameChapter);
        chapterBooks.setNumberChapter(numberChapter);
        Books books = bookServicesRepository.findById(idBook).get();
        chapterBooks.setBooks(books);
        chapterServicesRepository.save(chapterBooks);
        return new RedirectView("/home/book/" + idBook);
    }

    @GetMapping("/home/book/{idBook}/{numberChapter}")
    public ModelAndView viewChapterBook(ModelAndView modelAndView,
                                        @PathVariable Long idBook,
                                        @PathVariable int numberChapter) throws Exception {
        Collection<DescriptionBooks> descriptionBooks = descriptionBooksServicesRepository.findAllByCreateDateBetween();
        modelAndView.addObject("newBooks", descriptionBooks);
        ChapterBooks chapterBooks = null;
        try {
            chapterBooks = chapterServicesRepository.findByNumberChapterAndIdBook(numberChapter, idBook);
            modelAndView.addObject("chapter", chapterBooks);
        } catch (Exception e) {
            chapterBooks.setNumberChapter(numberChapter + 1);
            chapterBooks.setIdBook(idBook);
            modelAndView.addObject("chapter", chapterBooks);
            return modelAndView;
        }
        modelAndView.setViewName("chapter");
        return modelAndView;
    }


    @GetMapping("/delete/chapter/{idChapter}")
    public RedirectView deleteChapter(@PathVariable Long idChapter){
        chapterServicesRepository.deleteById(idChapter);
        return new RedirectView("/home?page=0&size=5");
    }
}
