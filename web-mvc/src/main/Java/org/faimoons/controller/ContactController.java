package org.faimoons.controller;

import org.faimoons.model.*;
import org.faimoons.services.interfaceServices.BookServicesRepository;
import org.faimoons.services.interfaceServices.ChatServiceRepository;
import org.faimoons.services.interfaceServices.ContactServicesRepository;
import org.faimoons.services.interfaceServices.DescriptionBooksServicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.Optional;


@Controller
public class ContactController {

    @Autowired
    private ContactServicesRepository contactServicesRepository;

    @Autowired
    private DescriptionBooksServicesRepository descriptionBooksServicesRepository;

    @Autowired
    private BookServicesRepository bookServicesRepository;

    @Autowired
    private ChatServiceRepository chatServiceRepository;

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public ModelAndView login(ModelAndView modelAndView, @RequestParam(required = false) String error) {
        if (error != null) {
            modelAndView.addObject("errorSing", error);
        }
        return modelAndView;
    }

    @RequestMapping(path = "/user/books", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView userBooks(ModelAndView modelAndView, Principal principal) {
        Contact contact = contactServicesRepository.findByLogin(principal.getName());
        modelAndView.addObject("newBooks", descriptionBooksServicesRepository.findAllByCreateDateBetween());
        modelAndView.addObject("books", contact.getDescriptionBooks());
        modelAndView.setViewName("booksUser");
        return modelAndView;
    }

    @PostMapping("/user/books/{idBook}")
    @ResponseBody
    public RedirectView userBooksP(@PathVariable Long idBook,
                                   Principal principal) throws NoSuchAlgorithmException {
        Contact contact = contactServicesRepository.findByLogin(principal.getName());
        if (contact.getDescriptionBooks().stream().noneMatch(e -> e.getIdBook().equals(idBook))) {
            Optional<DescriptionBooks> optional = descriptionBooksServicesRepository.findById(idBook);
            contact.addDescriptionBooks(optional.get());
            contactServicesRepository.save(contact);
            return new RedirectView("/user/books");
        } else return new RedirectView("/user/books");
    }

    @RequestMapping(path = "/user/edit", method = RequestMethod.GET)
    public ModelAndView userEdit(ModelAndView modelAndView,
                                 @RequestParam(required = false) String error,
                                 Principal principal) {
        if (error != null) {
            modelAndView.addObject("errorEdit", error);
        }
        modelAndView.addObject("newBooks", descriptionBooksServicesRepository.findAllByCreateDateBetween());
        modelAndView.addObject("contact", contactServicesRepository.findByLogin(principal.getName()));
        modelAndView.setViewName("editProfile");
        return modelAndView;
    }

    @PostMapping("/user/edit")
    public RedirectView userEdit(ModelAndView modelAndView,
                                 Principal principal,
                                 @RequestParam String firstName,
                                 @RequestParam String lastName,
                                 @RequestParam String psw,
                                 @RequestParam String psw_repeat) throws NoSuchAlgorithmException {
        if (passwordsMatch(psw, psw_repeat)) {
            Contact contact = contactServicesRepository.findByLogin(principal.getName());
            contact.setFirstName(firstName);
            contact.setLastName(lastName);
            contact.setPassword(psw);
            contactServicesRepository.save(contact);
        } else {
            modelAndView.addObject("errorPassword", "error");
            return new RedirectView("/user/edit");
        }
        return new RedirectView("/home?page=0&size=5");
    }

    @PostMapping("/home/book/{idBook}/message")
    public RedirectView createMessageUser(@PathVariable Long idBook,
                                          @RequestParam String message,
                                          Principal principal) {
        Contact contact = contactServicesRepository.findByLogin(principal.getName());
        Books books = bookServicesRepository.findById(idBook).get();
        Chat chat = new Chat();
        chat.setMessage(message);
        chat.setNameUser(contact.getFirstName());
        chat.setContact(contact);
        chat.setBooksChat(books);
        chatServiceRepository.save(chat);
        return new RedirectView("/home/book/" + idBook);
    }


    @GetMapping("/register")
    public ModelAndView viewRegister(ModelAndView modelAndView, @RequestParam(required = false) String error) {
        if (error != null) {
            modelAndView.addObject("errorRegister", error);
        }
        return modelAndView;
    }


    @PostMapping("/register")
    public ModelAndView createUser(ModelAndView modelAndView,
                                   @RequestParam String email,
                                   @RequestParam String psw,
                                   @RequestParam String login,
                                   @RequestParam String lastName,
                                   @RequestParam String firstName,
                                   @RequestParam String psw_repeat) throws NoSuchAlgorithmException {

        if (passwordsMatch(psw, psw_repeat)) {
            if (userAlreadyExistsWithSuchAnLogin(login)) {
                Contact contact = new Contact();
                contact.setEmail(email);
                contact.setFirstName(firstName);
                contact.setLastName(lastName);
                contact.setPassword(psw);
                contact.setLogin(login);
                Role role = new Role();
                role.setIdRole(3L);
                contact.setRole(role);
                modelAndView.setViewName("login");
                contactServicesRepository.save(contact);
            } else {
                modelAndView.addObject("errorRegister", "error");
                modelAndView.setViewName("register");
            }
        } else {
            modelAndView.addObject("errorPassword", "error");
            modelAndView.setViewName("register");
        }
        return modelAndView;
    }

    @GetMapping("/delete")
    public RedirectView deleteUser(Principal principal) {
        Contact contact = contactServicesRepository.findByLogin(principal.getName());
        contactServicesRepository.delete(contact);
        return new RedirectView("/logout");
    }

    @GetMapping("/delete/favorites/book/{idBook}/user")
    public RedirectView deleteFavoritesUser(Principal principal,
                                            @PathVariable Long idBook) throws NoSuchAlgorithmException {
        Contact contact = contactServicesRepository.findByLogin(principal.getName());
        contact.getDescriptionBooks().remove(descriptionBooksServicesRepository.findById(idBook).get());
        contactServicesRepository.save(contact);
        return new RedirectView("/user/books");
    }

    private boolean userAlreadyExistsWithSuchAnLogin(String login) {
        if (contactServicesRepository.findByLogin(login) == null) {
            return true;
        } else return false;
    }

    private boolean passwordsMatch(String psw, String psw_repeat) {
        return psw.equals(psw_repeat);
    }
}
