<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
</head>
<body>
<style>
    .tabcontent3 {
        margin-bottom: 20px;
        color: white;
        padding: 20px 20%;
        height: 10%;
    }

    .tabcontent4 {
        float: top;
        font-size: 20px;
        margin-bottom: 15px;
        color: white;
        padding: 2px 5px;
        height: 40%;
    }

    .sidenav2 {
        margin-right: 1%;
        margin-top: 2%;
        height: 100%;
        width: 300px;
        position: fixed;
        z-index: 1;
        float: left;
        top: 0;
        left: 75%;
        background-color: #111;
        overflow-x: hidden;
        padding-top: 20px;
        border-radius: 15px;
    }

    .sidenav2 a {
        padding: 6px 8px 6px 16px;
        text-decoration: none;
        font-size: 25px;
        color: #818181;

    }

    .sidenav2 a:hover {
        color: #f1f1f1;
    }

    .hr-circle {
        margin: 20px 0;
        padding: 0;
        height: 0;
        border: none;
        border-width: 0 0 5px;
        border-style: solid;
        border-image: url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2 1" width="8" height="4"><circle fill="orange" cx="1" cy="0.5" r="0.5"></circle></svg>') 0 0 100% repeat;
        background-position: 50%;
        box-sizing: border-box;
        color: orange;
    }

    a.button83 {
        font-size: 15px;
        margin-top: 5%;
        display: inline-block;
        color: #FDA101;
        width: 50%;
        margin-left: 20%;
        text-decoration: none;
        user-select: none;
        padding: .4em 2em;
        outline: none;
        border: 2px solid;
        border-radius: 10px;
        transition: 0.2s;
        font-size: 17px;
    }

    .hju {
        margin-left: 5%;
    }

    a.button83:hover {
        background: rgba(255, 255, 255, .2);
    }

    a.button83:active {
        background: white;
    }
</style>
<div class="sidenav2">
    <div class="tabcontent3">
        <h2>News Books</h2>
    </div>
    <c:forEach items="${newBooks}" var="newBooks">
        <div class="tabcontent4">
            <p class="hju">${newBooks.nameBook}</p>
            <img src="data:image/jpeg;base64,${newBooks.image}" class="logo1-img">
            <a href="${pageContext.servletContext.contextPath}/home/book/${newBooks.idBook} " class="button83"
               align="center">Read</a>
            <hr class="hr-circle">
        </div>
    </c:forEach>
</div>
</body>
</html>
