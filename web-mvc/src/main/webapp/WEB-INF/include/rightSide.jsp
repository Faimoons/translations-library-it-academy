<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/security/tags" %>
<head>
</head>
<body>
<c:url var="loginUrl" value="/login"/>
<div class="sidenav">
<div class="logo">
    <img src="${pageContext.servletContext.contextPath}/static/image/logo.png" class="logo-img">
</div>
<div class="dropdown">
    <button onclick="myFunction()" class="dropbtn">Menu</button>
    <div id="myDropdown" class="dropdown-content">
        <a href="${pageContext.servletContext.contextPath}/home?page=0&size=5">Main</a>
        <s:authorize access="hasRole('ROLE_ADMIN')">
            <a href="${pageContext.servletContext.contextPath}/logout">Logout</a>
            <a href="${pageContext.servletContext.contextPath}/delete">Delete</a>
        </s:authorize>
        <s:authorize access="hasRole('ROLE_USER')">
            <a href="${pageContext.servletContext.contextPath}/logout">Logout</a>
            <a href="${pageContext.servletContext.contextPath}/delete">Delete</a>
        </s:authorize>
    </div>
</div>
    <s:authorize access="hasRole('ROLE_ADMIN')">
        <a href="${pageContext.servletContext.contextPath}/user/edit"><img
                src="${pageContext.servletContext.contextPath}/static/image/img_avatar.png" class="logo-img"></a>
        <a href="${pageContext.servletContext.contextPath}/user/books"><img
                src="${pageContext.servletContext.contextPath}/static/image/2465.jpg" class="l1ogo-img"></a>
        <a href="${pageContext.servletContext.contextPath}/home/create/book"><img
                src="${pageContext.servletContext.contextPath}/static/image/add.png" class="l1ogo-img"></a>
    </s:authorize>
    <s:authorize access="hasRole('ROLE_USER')">
        <a href="${pageContext.servletContext.contextPath}/user/edit"><img
                src="${pageContext.servletContext.contextPath}/static/image/img_avatar.png" class="logo-img"></a>
        <a href="${pageContext.servletContext.contextPath}/user/books"><img
                src="${pageContext.servletContext.contextPath}/static/image/2465.jpg" class="l1ogo-img"></a>
    </s:authorize>
    <s:authorize access="isAnonymous()">
        <div class="dropdown">
            <button onclick="document.getElementById('id01').style.display='block'" class="dropbtn2">Sing in</button>
        </div>
        <div id="id01" class="modal">
            <form class="modal-content animate" action="${loginUrl}" method="post">
                <div class="imgcontainer">
    <span onclick="document.getElementById('id01').style.display='none'" class="close"
          title="Close Modal">×</span>
                    <img src="../../static/image/img_avatar2.png" alt="Avatar" class="avatar">
                </div>
                <div class="container3">
                    <label for="login"><b>Login</b></label>
                    <input type="text" placeholder="Enter Username" name="username" required>
                    <label for="psw"><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" name="password" required>
                    <button type="submit">Login</button>
                </div>
            </form>
        </div>
        <div class="dropdown">
            <button onclick="document.getElementById('id02').style.display='block'" class="dropbtn3">Register</button>
        </div>
        <div id="id02" class="modal">
    <span onclick="document.getElementById('id02').style.display='none'" class="close"
          title="Close Modal">×</span>
            <form class="modal-content" action="${pageContext.servletContext.contextPath}/register" method="post">
                <div class="imgcontainer">
                    <a href="${pageContext.servletContext.contextPath}/home?page=0&size=5" class="close">×</a>
                    <img src="../../static/image/img_avatar2.png" alt="Avatar" class="avatar">
                </div>
                <div class="container3">
                    <h1>Sign Up</h1>
                    <p>Please fill in this form to create an account.</p>
                    <hr>
                    <label for="name"><b>Name</b></label>
                    <input type="text" placeholder="Name" name="firstName" required>
                    <label for="lastName"><b>Last name</b></label>
                    <input type="text" placeholder="Last name" name="lastName" required>
                    <label for="login"><b>Login</b></label>
                    <input type="text" placeholder="login" name="login" required>
                    <label for="email"><b>Email</b></label>
                    <input type="text" placeholder="Enter Email" name="email" required>
                    <label for="psw"><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" name="psw" required>
                    <label for="psw-repeat"><b>Repeat Password</b></label>
                    <input type="password" placeholder="Repeat Password" name="psw_repeat" required>
                    <c:if test="${errorRegister != null}">
                        <b>
                            <p style="color:red" class="ip">Enter the fields correctly</p>
                        </b>
                    </c:if>
                    <c:if test="${errorPassword != null}">
                        <b>
                            <p style="color:red" class="ip">Passwords don't matchy</p>
                        </b>
                    </c:if>
                    <div class="clearfix">
                        <button type="submit" class="signupbtn">Sign Up</button>
                    </div>
                </div>
            </form>
        </div>
    </s:authorize>
    </div>
    </body>