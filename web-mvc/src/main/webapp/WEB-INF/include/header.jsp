<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>
    <link href="${pageContext.request.contextPath}static/styles/footer.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="footer">
    <h2>© 2018–2020 «Books»
    </h2>
</div>
</body>
