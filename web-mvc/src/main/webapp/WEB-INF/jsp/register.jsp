<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="${pageContext.request.contextPath}static/styles/homeView.css" rel="stylesheet" type="text/css">
</head>
<body>
<form class="modal-content animate" action="${pageContext.servletContext.contextPath}/register" method="post">
    <div class="imgcontainer">
        <a href="${pageContext.servletContext.contextPath}/home?page=0&size=5" class="close">×</a>
        <img src="${pageContext.request.contextPath}static/image/img_avatar2.png" alt="Avatar" class="avatar">
    </div>
    <div class="container3">
        <h1>Sign Up</h1>
        <p>Please fill in this form to create an account.</p>
        <hr>
        <label for="name"><b>Name</b></label>
        <input type="text" placeholder="Name" name="firstName" required>
        <label for="lastName"><b>Last name</b></label>
        <input type="text" placeholder="Last name" name="lastName" required>
        <label for="login"><b>Login</b></label>
        <input type="text" placeholder="login" name="login" required>
        <label for="email"><b>Email</b></label>
        <input type="text" placeholder="Enter Email" name="email" required>
        <label for="psw"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="psw" required>
        <label for="psw-repeat"><b>Repeat Password</b></label>
        <input type="password" placeholder="Repeat Password" name="psw_repeat" required>
        <c:if test="${errorRegister != null}">
            <b>
                <p style="color:red" class="ip">This username already exists</p>
            </b>
        </c:if>
        <c:if test="${errorPassword != null}">
            <b>
                <p style="color:red" class="ip">Passwords don't matchy</p>
            </b>
        </c:if>
        <div class="clearfix">
            <button type="submit" class="signupbtn">Sign Up</button>
        </div>
    </div>
</form>
</body>
</html>