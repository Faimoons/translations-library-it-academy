<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="${pageContext.request.contextPath}/static/styles/createbook.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/static/styles/footer.css" rel="stylesheet" type="text/css">
</head>
<body>
<style>
    .hr-circle {
        margin: 20px 0;
        padding: 0;
        height: 0;
        border: none;
        border-width: 0 0 5px;
        border-style: solid;
        border-image: url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2 1" width="8" height="4"><circle fill="orange" cx="1" cy="0.5" r="0.5"></circle></svg>') 0 0 100% repeat;
        background-position: 50%;
        box-sizing: border-box;
        color: orange;
    }
    textarea {
        background: #f1f1f1; /* Цвет фона */
        border: 2px solid #a9c358; /* Параметры рамки */
        padding: 10px; /* Поля */
        width: 100%; /* Ширина */
        height: 200px; /* Высота */
        box-sizing: border-box; /* Алгоритм расчёта ширины */
        font-size: 14px; /* Размер шрифта */
    }
    input[type=text], input[type=file], input[type=date], input[type=password], input[type=number] {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
    }
    input[type=text]:focus, input[type=file]:focus, input[type=date]:focus, input[type=password]:focus, input[type=number]:focus {
        background-color: #ddd;
        outline: none;
    }
</style>
<jsp:include page="../include/rightSide.jsp"/>
<jsp:include page="../include/leftSide.jsp"/>
<div class="main">
    <div class="row">
        <div class="op">
            <h2 class="hj" align="center">Faimoons</h2>
        </div>
        <hr class="hr-circle">
        <div class="card">
            <form action="${pageContext.servletContext.contextPath}/home/book/${idBook}/create/chapter"
                  method="post">
                <div class="container">
                    <h3>Create chapter book</h3>
                    <hr>
                    <label for="nameChapter"><b>Name</b></label>
                    <input type="text" placeholder="Name" name="nameChapter" required>
                    <label for="numberChapter"><b>Number chapter</b></label>
                    <input type="number" placeholder="1" name="numberChapter" required>
                    <label for="chapter"><b>Chapter</b></label>
                    <textarea name="chapter"></textarea>
                    <hr>
                    <button type="submit" class="registerbtn">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
<jsp:include page="../include/header.jsp"/>
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    var modal = document.getElementById('id01');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }
    // Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
</script>
</body>
</html>
