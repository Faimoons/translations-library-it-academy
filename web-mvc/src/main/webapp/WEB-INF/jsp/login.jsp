<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="${pageContext.request.contextPath}static/styles/homeView.css" rel="stylesheet" type="text/css">
</head>
<body>
<c:url var="loginUrl" value="/login"/>
<form class="modal-content animate" action="${loginUrl}" method="post">
    <div class="imgcontainer">
        <a href="${pageContext.servletContext.contextPath}/home?page=0&size=5" class="close">×</a>
        <img src="${pageContext.request.contextPath}static/image/img_avatar2.png" alt="Avatar" class="avatar">
    </div>
    <div class="container3">
        <label for="username"><b>Username</b></label>
        <input type="text" placeholder="Enter Login" id="username" name="username" required>
        <label for="password"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" id="password" name="password" required>
        <c:if test="${errorSing != null}">
            <b>
                <p style="color:red" class="ip">Enter the fields correctly</p>
            </b>
        </c:if>
        <button type="submit">Login</button>
    </div>
</form>
</body>
</html>
