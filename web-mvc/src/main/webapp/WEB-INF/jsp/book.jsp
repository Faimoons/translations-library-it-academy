<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <link href="../../static/styles/book.css" rel="stylesheet" type="text/css">
    <link href="../../static/styles/homeView.css" rel="stylesheet" type="text/css">
</head>
<body>
<style>
    .hr-circle {
        margin: 20px 0;
        padding: 0;
        height: 0;
        border: none;
        border-width: 0 0 5px;
        border-style: solid;
        border-image: url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2 1" width="8" height="4"><circle fill="orange" cx="1" cy="0.5" r="0.5"></circle></svg>') 0 0 100% repeat;
        background-position: 50%;
        box-sizing: border-box;
        color: orange;
    }

    textarea {
        background: #f1f1f1; /* Цвет фона */
        border: 2px solid #a9c358; /* Параметры рамки */
        padding: 10px; /* Поля */
        width: 100%; /* Ширина */
        height: 50px; /* Высота */
        box-sizing: border-box; /* Алгоритм расчёта ширины */
        font-size: 14px; /* Размер шрифта */
    }

    .registerbtn33 {
        background-color: #312F41;
        color: #FDA101;
        padding: 10px 15px;
        margin: 8px 0;
        border: 2px solid;
        border-radius: 10px;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
        font-size: 17px;
    }

    .registerbtn33:hover {
        background: rgba(255, 255, 255, .2);
    }

    .registerbtn33:active {
        background: white;
    }
</style>
<div class="header">
    <h1>Books Library</h1>
    <p>A website created by me.</p>
</div>
<jsp:include page="../include/rightSide.jsp"/>
<jsp:include page="../include/leftSide.jsp"/>
<div class="main">
    <div class="row">
        <div class="oop">
            <h2 class="hj" align="center">${book.descriptionBooks.nameBook}</h2>
        </div>
        <hr class="hr-circle">
        <div class="card">
            <div class="image">
                <img style="width:100%" src="data:image/jpeg;base64,${book.descriptionBooks.image}"/>
                <s:authorize access="hasRole('ROLE_USER')">
                    <a href="${pageContext.servletContext.contextPath}/home/book/${book.idBook}/1"
                       class="button8" align="center">Start reading</a>
                </s:authorize>
                <s:authorize access="hasRole('ROLE_ADMIN')">
                    <a href="${pageContext.servletContext.contextPath}/delete/book/${book.idBook}"
                       class="button8" align="center">Delete book</a>
                    <a href="${pageContext.servletContext.contextPath}/home/book/${book.idBook}/create/chapter"
                       class="button8" align="center">Add a Chapter</a>
                </s:authorize>
                <form action="${pageContext.servletContext.contextPath}/user/books/${book.idBook}"
                      method="post">
                    <button type="submit" class="registerbtn33">To favorites</button>
                </form>
            </div>
            <div class="container323">
                <div class="err">
                    <p>Genre :</p>
                    <p>${book.genre}</p>
                </div>
                <div class="err">
                    <p>Aurhor :</p>
                    <p>${book.author}</p>
                </div>
                <div class="err">
                    <p>Country :</p>
                    <p>${book.country}</p>
                </div>
                <div class="err">
                    <p>Published :</p>
                    <p>${book.dateWriting}</p>
                </div>
            </div>
            <div class="vertical-menu">
                <hr class="hr-circle">
                <a href="#" class="active">Chapters</a>
                <c:forEach items="${countChapter}" var="countChapter">
                    <a href="${pageContext.servletContext.contextPath}/home/book/${book.idBook}/${countChapter.toString()}">${countChapter.toString()}</a>
                </c:forEach>
                <c:if test="${countChapter == null}">
                    <a href="#">Wait for the chapter to come out</a>
                </c:if>
            </div>
        </div>
        <h2 class="hj" align="center">Chat Book</h2>
        <form action="${pageContext.servletContext.contextPath}/home/book/${book.idBook}/message" method="post">
            <textarea name="message" placeholder="Your message"></textarea>
            <button type="submit" class="registerbtn">send</button>
        </form>
        <c:forEach items="${message}" var="message">
            <div class="container45">
                <p>User :${message.nameUser}</p>
                <p>${message.message}</p>
                <span class="time-right">${message.timeMessage}</span>
            </div>
        </c:forEach>
    </div>
</div>
<jsp:include page="../include/header.jsp"/>
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    var modal = document.getElementById('id01');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }


    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }

    // Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
</script>

</body>
</html>
