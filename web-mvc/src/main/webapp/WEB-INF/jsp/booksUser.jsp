<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <link href="../../static/styles/footer.css" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../../static/styles/homeView.css" rel="stylesheet" type="text/css">
</head>

<body>
<style>
    .hr-circle {
        margin: 20px 0;
        padding: 0;
        height: 0;
        border: none;
        border-width: 0 0 5px;
        border-style: solid;
        border-image: url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2 1" width="8" height="4"><circle fill="orange" cx="1" cy="0.5" r="0.5"></circle></svg>') 0 0 100% repeat;
        background-position: 50%;
        box-sizing: border-box;
        color: orange;
    }
    .derip {
        text-decoration: none;
        color: #DCD1D1;
    }
    .derip:hover {
        color: orange;
    }
</style>
<jsp:include page="../include/rightSide.jsp"/>
<jsp:include page="../include/leftSide.jsp"/>
<div class="main">
    <div class="row">
        <div class="oop">
            <h2 class="hj" align="center">
                <s:authentication property="principal.username" /></h2>
        </div>
        <hr class="hr-circle">
        <c:forEach items="${books}" var="books">
            <div class="card">
                <div class="image">
                    <img style="width:100%" src="data:image/jpeg;base64,${books.image}"/>
                    <a href="${pageContext.servletContext.contextPath}/home/book/${books.idBook} "
                       class="button8" align="center">Read</a>
                    <a href="${pageContext.servletContext.contextPath}/delete/favorites/book/${books.idBook}/user "
                       class="button8" align="center">Delete from favorites</a>
                    <s:authorize access="hasRole('ROLE_ADMIN')">
                        <a href="${pageContext.servletContext.contextPath}/home/book/${books.idBook}/create/chapter"
                           class="button8" align="center">Add a Chapter</a>
                    </s:authorize>
                </div>
                <div class="container">
                    <h2 class="p"><a href="${pageContext.servletContext.contextPath}/home/book/${books.idBook} "
                                     class="derip" align="center">${books.nameBook}</a></h2>
                    <p class="g">
                            ${books.description}
                </div>
            </div>
        </c:forEach>
    </div>
</div>
</div>
<div class="footer">
    <h2>© 2018–2020 «Books»
    </h2>
</div>
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    var modal = document.getElementById('id01');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }
    // Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
</script>

</body>
</html>
