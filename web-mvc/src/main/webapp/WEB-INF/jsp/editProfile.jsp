<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="../../static/styles/homeView.css" rel="stylesheet" type="text/css">
    <link href="../../static/styles/footer.css" rel="stylesheet" type="text/css">
</head>
<body>
<style>
    .hr-circle {
        margin: 20px 0;
        padding: 0;
        height: 0;
        border: none;
        border-width: 0 0 5px;
        border-style: solid;
        border-image: url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2 1" width="8" height="4"><circle fill="orange" cx="1" cy="0.5" r="0.5"></circle></svg>') 0 0 100% repeat;
        background-position: 50%;
        box-sizing: border-box;
        color: orange;
    }
</style>
<jsp:include page="../include/rightSide.jsp"/>
<jsp:include page="../include/leftSide.jsp"/>
<div class="main">
    <div class="row">
        <div class="oop">
            <h2 class="hj" align="center">Books</h2>
        </div>
        <hr class="hr-circle">
        <div class="card">
            <form action="${pageContext.servletContext.contextPath}/user/edit" method="post">
                <div class="container12">
                    <h3 class="tyu">Edit Profile</h3>
                    <hr class="hr-circle">
                    <label for="firstName" class="tyu"><b>First name</b></label>
                    <input type="text" placeholder="firstName" name="firstName" value="${contact.firstName}" required>
                    <label for="lastName" class="tyu"><b>Last name</b></label>
                    <input type="text" placeholder="lastName" name="lastName" value="${contact.lastName}" required>
                    <label for="psw" class="tyu"><b>Password</b></label>
                    <input type="password" placeholder="Enter Password" name="psw" required>
                    <label for="psw-repeat" class="tyu"><b>Repeat Password</b></label>
                    <input type="password" placeholder="Repeat Password" name="psw_repeat" required>
                    <c:if test="${errorPassword != null}">
                        <b>
                            <p style="color:red" class="ip">Passwords don't matchy</p>
                        </b>
                    </c:if>
                    <hr class="hr-circle">
                    <button type="submit" class="registerbtn">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="footer">
    <h2>© 2018–2020 «Books»
    </h2>
</div>
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    var modal = document.getElementById('id01');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }
    // Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
</script>
</body>
</html>
