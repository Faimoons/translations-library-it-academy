<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="${pageContext.request.contextPath}static/styles/homeView.css" rel="stylesheet" type="text/css">
    <link href="../../static/styles/homeView.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}static/styles/footer.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}static/styles/pagination.css" rel="stylesheet" type="text/css">
</head>
<body>
<style>

    .p {
        font-size: 35px;
        color: #DCD1D1;
    }

    .derip {
        text-decoration: none;
        color: #DCD1D1;
    }

    .derip:hover {
        color: orange;
    }
    .pagination a.active {
        background-color: #FF7518;
        color: white;
        border-radius: 10px;
    }
</style>
<div class="header">
    <h1>Books Library</h1>
    <p>A website created by me.</p>
</div>
<jsp:include page="../include/rightSide.jsp"/>
<jsp:include page="../include/leftSide.jsp"/>
<div class="main">
    <div class="row">
        <form class="example" action="${pageContext.servletContext.contextPath}/home" method="post">
            <input type="text" placeholder="Search.." name="search">
            <button type="submit"><i class="fa fa-search"></i></button>
        </form>
        <c:forEach items="${books}" var="books">
            <div class="card">
                <div class="image">
                    <img style="width:100%" src="data:image/jpeg;base64,${books.image}"/>
                    <a href="${pageContext.servletContext.contextPath}/home/book/${books.idBook} "
                       class="button8" align="center">Read</a>
                    <s:authorize access="hasRole('ROLE_ADMIN')">
                        <a href="${pageContext.servletContext.contextPath}/home/book/${books.idBook}/create/chapter"
                           class="button8" align="center">Add a Chapter</a>
                    </s:authorize>
                </div>
                <div class="container">
                    <h2 class="p"><a href="${pageContext.servletContext.contextPath}/home/book/${books.idBook} "
                                     class="derip" align="center">${books.nameBook}</a></h2>
                    <p class="g">
                            ${books.description}
                </div>
            </div>
        </c:forEach>
<c:if test="${noPageable != null}">
        <div class="teer">
            <div class="pagination">
                <c:if test="${numberPage != 0}">
                    <a href="${pageContext.servletContext.contextPath}/home?page=${numberPage-1}&size=5">«</a>
                </c:if>
                <c:forEach items="${totalPage}" var="totalPage">
                    <c:set var="page" value="${totalPage.toString()}"/>
                    <c:if test="${numberPage == page}">
                        <a class="active" href="${pageContext.servletContext.contextPath}/home?page=${numberPage}&size=5">${totalPage.toString()+1}</a>
                    </c:if>
                    <c:if test="${numberPage != page}">
                        <a href="${pageContext.servletContext.contextPath}/home?page=${totalPage.toString()}&size=5">${totalPage.toString()+1}</a>
                    </c:if>
                </c:forEach>
                <a href="${pageContext.servletContext.contextPath}/home?page=${numberPage+1}&size=5">»</a>
            </div>
        </div>
        </c:if>
    </div>
</div>
<div class="footer">
    <h2>© 2018–2020 «Books»
    </h2>
</div>
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    var modal = document.getElementById('id01');

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }
    // Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
</script>
</body>
</html>

