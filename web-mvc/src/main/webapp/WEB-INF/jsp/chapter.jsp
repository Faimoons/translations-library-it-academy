<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="${pageContext.servletContext.contextPath}/static/styles/homeView.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/static/styles/chapterBook.css" rel="stylesheet"
          type="text/css">
</head>
<body>
<style>
    .hr-circle {
        margin: 20px 0;
        padding: 0;
        height: 0;
        border: none;
        border-width: 0 0 5px;
        border-style: solid;
        border-image: url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 2 1" width="8" height="4"><circle fill="orange" cx="1" cy="0.5" r="0.5"></circle></svg>') 0 0 100% repeat;
        background-position: 50%;
        box-sizing: border-box;
        color: orange;
    }

    .round {
        border-radius: 5px;
    }

    .vertical-menu {
        width: 100%; /* Set a width if you like */
    }

    .vertical-menu a {
        background-color: #312F41; /* Grey background color */
        color: white; /* Black text color */
        display: block; /* Make the links appear below each other */
        padding: 12px; /* Add some padding */
        text-decoration: none; /* Remove underline from links */
        margin-bottom: -40px;
        margin-top: 35px;
        font-size: 40px;
    }

    .vertical-menu a:hover {
        background-color: #ccc; /* Dark grey background on mouse-over */
    }

    .vertical-menu a.active {
        background-color: black; /* Add a green color to the "active/current" link */
        color: white;
    }
</style>
<div class="header">
    <h1>Chapter is ${chapter.nameChapter}</h1>
    <p>A website created by me.</p>
</div>
<jsp:include page="../include/rightSide.jsp"/>
<jsp:include page="../include/leftSide.jsp"/>
<c:set var="firstChapter" value="1"/>
<c:if test="${chapter.numberChapter == null}">
    <div class="main">
        <hr class="hr-circle">
        <div class="row">
            <h2 align="center" class="fig">Chapter not found or not translated</h2>
            <h2 align="center" class="fig">Please accept our apologies</h2>
            <hr class="hr-circle">
        </div>
    </div>
</c:if>
<c:if test="${chapter.numberChapter != null}">
    <div class="main">
        <hr class="hr-circle">
        <div class="row">
            <h2 align="center" class="fig">Chapter ${chapter.numberChapter} ${chapter.nameChapter} </h2>
            <hr class="hr-circle">
            <div class="card">
                <div class="container234">
                    <s:authorize access="hasRole('ROLE_ADMIN')">
                        <div class="vertical-menu">
                            <a href="${pageContext.servletContext.contextPath}/home/book/chapter/edit/${chapter.idChapter}"
                               align="center">Edit chapter</a>
                        </div>
                        <div class="vertical-menu">
                            <a href="${pageContext.servletContext.contextPath}/delete/chapter/${chapter.idChapter}"
                               align="center">Delete chapter</a>
                        </div>
                    </s:authorize>
                    <p class="kj">
                            ${chapter.chapter}
                    </p>
                    <hr class="hr-circle">
                    <c:if test="${chapter.numberChapter != firstChapter}">
                        <a href="${pageContext.servletContext.contextPath}/home/book/${chapter.idBook}/${chapter.numberChapter-1}"
                           class="previous round">‹</a>
                    </c:if>
                    <a href="${pageContext.servletContext.contextPath}/home/book/${chapter.idBook}"
                       class="previous2 round">Contents</a>
                    <a href="${pageContext.servletContext.contextPath}/home/book/${chapter.idBook}/${chapter.numberChapter+1}"
                       class="next round">›</a>
                </div>
            </div>
        </div>
    </div>
</c:if>
<jsp:include page="../include/header.jsp"/>
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    var modal = document.getElementById('id01');
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
    function myFunction() {
        document.getElementById("myDropdown").classList.toggle("show");
    }
    // Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {

            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
</script>

</body>
</html>
