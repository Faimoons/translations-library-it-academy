package org.faimoons;

import org.faimoons.model.Contact;
import org.faimoons.model.DescriptionBooks;
import org.faimoons.repository.*;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring-context.xml"})
public class testSpring {

    @Autowired
    private DescriptionBooksRepository descriptionBooks;


    @Autowired
    private ChapterBookRepository chapterBookRepository;


    @Autowired
    private ContactRepository contactRepository;

    @Test
    public void testCacheForDescriptionBook() {
        Pageable pageable = PageRequest.of(0, 5);
        descriptionBooks.findAll(pageable);
        System.out.println("-------------------------------------------------------------------------------------");
        descriptionBooks.findAll(pageable).forEach(e -> System.out.println(e.getNameBook()));
        System.out.println("-------------------------------------------------------------------------------------");
        descriptionBooks.findAll(pageable).forEach(e -> System.out.println(e.getNameBook()));
    }

    // test for adding a book
    @Ignore
    @Test
    public void springTest2() {
        Contact contact = contactRepository.findByLogin("Test");
        Optional<DescriptionBooks> optional = this.descriptionBooks.findById(43L);
        DescriptionBooks descriptionBooks = optional.get();
        contact.addDescriptionBooks(descriptionBooks);

        contactRepository.save(contact);
    }

    // test search
    @Ignore
    @Test
    public void testSearchForSpring() {
        String searchWord = "farm";
        // To output the first five
        Pageable pageable = PageRequest.of(0, 5);
        Assert.assertEquals(descriptionBooks.
                findAllByNameBookContaining(searchWord, pageable).
                get().
                findFirst().
                get().
                getNameBook(), "Spatial farm in another world");
    }


    @Test
    public void testForFindingChapterByNumberAndIdBook() {
        Assert.assertNotNull(chapterBookRepository.findByNumberChapterAndIdBook(1, 38L));
    }


    @Test
    public void testForFindingBookById() {
        final Optional<org.faimoons.model.DescriptionBooks> byId = descriptionBooks.findById(38L);
        Assert.assertNotNull(byId.get());
    }

}
