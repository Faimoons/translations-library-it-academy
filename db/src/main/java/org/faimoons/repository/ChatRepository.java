package org.faimoons.repository;


import org.faimoons.model.Chat;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ChatRepository extends CrudRepository<Chat,Long> {

    List<Chat> findAllByIdBookOrderByTimeMessageDesc(Long idBook);
}
