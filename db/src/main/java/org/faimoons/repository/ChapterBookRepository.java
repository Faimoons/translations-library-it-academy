package org.faimoons.repository;

import org.faimoons.model.ChapterBooks;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChapterBookRepository extends CrudRepository<ChapterBooks,Long> {

    ChapterBooks findByNumberChapterAndIdBook(int numberChapter, Long idBook);

    @Query(value = "SELECT c.numberChapter FROM ChapterBooks AS c where c.idBook = :idBook order by c.numberChapter DESC")
    List<Integer> getAllNumberChapterForBook(@Param("idBook") Long idBook);

}
