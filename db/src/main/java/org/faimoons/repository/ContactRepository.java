package org.faimoons.repository;

import org.faimoons.model.Contact;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends CrudRepository<Contact,Long> {

    Contact findByLogin(String login);

}
