package org.faimoons.repository;

import org.faimoons.model.DescriptionBooks;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Collection;

@Repository
public interface DescriptionBooksRepository extends CrudRepository<org.faimoons.model.DescriptionBooks,Long> , PagingAndSortingRepository<DescriptionBooks, Long> {

    Collection<DescriptionBooks> findAllByCreateDateBetween(LocalDate createDate, LocalDate createDate2);

    Page<DescriptionBooks> findAllByNameBookContaining(String nameBook,Pageable pageable);

    Long countByNameBookContaining(String nameBook);

    Long countAllBy();

    @Override
    Page<DescriptionBooks> findAll(Pageable pageable);

    @Override
    Iterable<DescriptionBooks> findAll();
}
