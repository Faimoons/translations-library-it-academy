package org.faimoons;

import org.faimoons.model.Books;
import org.faimoons.model.Contact;
import org.faimoons.model.DescriptionBooks;
import org.faimoons.services.interfaceServices.ContactServicesRepository;
import org.faimoons.services.services.BookServicesRepositoryImp;
import org.faimoons.services.services.DescriptionBooksServicesRepositoryImp;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:services-spring-context.xml"})
public class TestSpringAutowired {

    @Autowired
    private DescriptionBooksServicesRepositoryImp descriptionBooksServicesRepository;

    @Autowired
    private BookServicesRepositoryImp bookServicesRepositoryImp;

    @Autowired
    private ContactServicesRepository contactServicesRepository;

    @Ignore
    @Test
    public void testAddInFavoritesUser() throws Exception {
        Optional<DescriptionBooks> books = descriptionBooksServicesRepository.findById(48L);
        Contact contact = contactServicesRepository.findByLogin("Test");
        contact.addDescriptionBooks(books.get());
        contactServicesRepository.save(contact);
    }

    @Test
    public void testFindBookSpring() throws Exception {
        Optional<Books> books = bookServicesRepositoryImp.findById(38L);
        Assert.assertNotNull(books.get());
    }

    @Test
    public void testFindContactSpring() throws Exception {
        Contact contact = contactServicesRepository.findByLogin("Test");
        Assert.assertNotNull(contact);
    }

}
