package org.faimoons.services.auth;

import org.faimoons.model.Contact;
import org.faimoons.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthService implements UserDetailsService {


    @Autowired
    private ContactRepository contactRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        final Contact contact = contactRepository.findByLogin(login);
        if (contact != null) {
            return new User(login, "{noop}"+contact.getPassword(), getAuthorities(contact));
        } else {
            throw new UsernameNotFoundException("User not found");
        }
    }

    private List<GrantedAuthority> getAuthorities(Contact contact) {
        final List<GrantedAuthority> authorities = new ArrayList<>();

        switch (contact.getRole().getName()) {
            case "admin":
                authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                break;
            case "user":
                authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
                break;
            default:
        }
//        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        return authorities;
    }

}
