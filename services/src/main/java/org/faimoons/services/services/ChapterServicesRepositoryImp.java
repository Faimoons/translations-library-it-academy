package org.faimoons.services.services;

import org.faimoons.model.ChapterBooks;
import org.faimoons.repository.ChapterBookRepository;
import org.faimoons.services.interfaceServices.ChapterServicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class ChapterServicesRepositoryImp  implements ChapterServicesRepository {

    @Autowired
    private ChapterBookRepository chapterBookRepository;

    public ChapterBooks findByNumberChapterAndIdBook(int numberChapter, Long idBook) {
        return chapterBookRepository.findByNumberChapterAndIdBook(numberChapter,idBook);
    }


    public List<Integer> getAllNumberChapterForBook(Long idBook) {
        return chapterBookRepository.getAllNumberChapterForBook(idBook);
    }


    public <S extends ChapterBooks> S save(S s) {
        return chapterBookRepository.save(s);
    }


    public <S extends ChapterBooks> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }


    public Optional<ChapterBooks> findById(Long aLong) {
        return chapterBookRepository.findById(aLong);
    }


    public boolean existsById(Long aLong) {
        return false;
    }


    public Iterable<ChapterBooks> findAll() {
        return null;
    }


    public Iterable<ChapterBooks> findAllById(Iterable<Long> iterable) {
        return null;
    }


    public long count() {
        return chapterBookRepository.count();
    }


    public void deleteById(Long aLong) {
chapterBookRepository.deleteById(aLong);
    }

    public void delete(ChapterBooks chapterBooks) {
chapterBookRepository.delete(chapterBooks);
    }


    public void deleteAll(Iterable<? extends ChapterBooks> iterable) {

    }


    public void deleteAll() {

    }
}
