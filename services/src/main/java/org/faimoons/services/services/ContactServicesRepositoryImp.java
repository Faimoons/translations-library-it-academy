package org.faimoons.services.services;


import org.faimoons.model.Contact;
import org.faimoons.repository.ContactRepository;
import org.faimoons.services.interfaceServices.ContactServicesRepository;
import org.faimoons.services.util.EncryptionMd5Imp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

@Service
public class ContactServicesRepositoryImp implements ContactServicesRepository {

    @Autowired
    private ContactRepository contactRepository;

    @Override
    public Contact findByLogin(String login) {
        return contactRepository.findByLogin(login);
    }

    @Override
    public <S extends Contact> S save(S s) throws NoSuchAlgorithmException {
//        s.setPassword(EncryptionMd5Imp.encryption(s.getPassword()));
        return contactRepository.save(s);
    }

    @Override
    public Optional<Contact> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public Iterable<Contact> findAll() {
        return null;
    }

    @Override
    public Iterable<Contact> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return contactRepository.count();
    }

    @Override
    public void deleteById(Long aLong) {
        contactRepository.deleteById(aLong);

    }

    @Override
    public void delete(Contact contact) {
        contactRepository.delete(contact);
    }
}
