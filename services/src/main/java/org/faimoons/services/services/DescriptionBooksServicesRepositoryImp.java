package org.faimoons.services.services;

import org.faimoons.model.DescriptionBooks;
import org.faimoons.repository.DescriptionBooksRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;


import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class DescriptionBooksServicesRepositoryImp implements org.faimoons.services.interfaceServices.DescriptionBooksServicesRepository {


    @Autowired
    private DescriptionBooksRepository descriptionBooksRepository;

    @Override
    public Iterable<DescriptionBooks> findAll(Sort sort) {
        return descriptionBooksRepository.findAll(sort);
    }

    @Override
    public Page<DescriptionBooks> findAll(Pageable pageable) {
        return descriptionBooksRepository.findAll(pageable);
    }

    @Override
    public Collection<DescriptionBooks> findAllByCreateDateBetween() {
        LocalDate localDate = LocalDate.now();
        return descriptionBooksRepository.findAllByCreateDateBetween(localDate.minusDays(30),localDate);
    }

    @Override
    public Collection<DescriptionBooks> findAllByNameBookContaining(String nameBook) {
        Pageable pageable = PageRequest.of(0,10);
        return descriptionBooksRepository.findAllByNameBookContaining(nameBook,pageable).toList();
    }

    @Override
    public Long countByNameBookContaining(String nameBook) {
        return null;
    }

    @Override
    public Long countAllBy() {
        return null;
    }


    @Override
    public <S extends DescriptionBooks> S save(S s) {
        s.setCreateDate(LocalDate.now());
        return descriptionBooksRepository.save(s);
    }

    @Override
    public <S extends DescriptionBooks> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<DescriptionBooks> findById(Long aLong) {
        return descriptionBooksRepository.findById(aLong);
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public List<DescriptionBooks> findAll() {
//        Pageable pageable = PageRequest.of(0,5);
//        Page<DescriptionBooks> all = descriptionBooksRepository.findAll(pageable);
//        return all.getContent();
        return null;
    }

    @Override
    public Iterable<DescriptionBooks> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(DescriptionBooks descriptionBooks) {

    }

    @Override
    public void deleteAll(Iterable<? extends DescriptionBooks> iterable) {

    }

    @Override
    public void deleteAll() {

    }
}
