package org.faimoons.services.services;

import org.faimoons.model.Books;
import org.faimoons.repository.BooksRepository;
import org.faimoons.services.interfaceServices.BookServicesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BookServicesRepositoryImp implements BookServicesRepository {

    @Autowired
    private BooksRepository booksRepository;

    @Override
    public <S extends Books> S save(S s) {
        return booksRepository.save(s);
    }

    @Override
    public <S extends Books> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Books> findById(Long aLong) {
        return booksRepository.findById(aLong);
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<Books> findAll() {
        return null;
    }

    @Override
    public Iterable<Books> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {
        booksRepository.deleteById(aLong);
    }

    @Override
    public void delete(Books books) {
        booksRepository.delete(books);
    }


    @Override
    public void deleteAll() {

    }
}
