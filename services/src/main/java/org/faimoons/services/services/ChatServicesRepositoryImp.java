package org.faimoons.services.services;

import org.faimoons.model.Chat;
import org.faimoons.repository.ChatRepository;
import org.faimoons.services.interfaceServices.ChatServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
@Service
public class ChatServicesRepositoryImp implements ChatServiceRepository {

    @Autowired
    private ChatRepository chatRepository;

    public List<Chat> findAllByIdBookOrderByTimeMessageDesc(Long idBook) {
        return chatRepository.findAllByIdBookOrderByTimeMessageDesc(idBook);
    }


    public <S extends Chat> S save(S s) {
        s.setTimeMessage(LocalDateTime.now());
        return chatRepository.save(s);
    }


    public <S extends Chat> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }


    public Optional<Chat> findById(Long aLong) {
        return chatRepository.findById(aLong);
    }


    public boolean existsById(Long aLong) {
        return false;
    }


    public Iterable<Chat> findAll() {
        return null;
    }


    public Iterable<Chat> findAllById(Iterable<Long> iterable) {
        return null;
    }


    public long count() {
        return chatRepository.count();
    }


    public void deleteById(Long aLong) {

    }


    public void delete(Chat chat) {
chatRepository.delete(chat);
    }


    public void deleteAll(Iterable<? extends Chat> iterable) {

    }


    public void deleteAll() {

    }
}
