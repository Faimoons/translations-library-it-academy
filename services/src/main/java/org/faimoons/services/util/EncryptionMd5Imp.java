package org.faimoons.services.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptionMd5Imp {


    public static String encryption(String password) throws NoSuchAlgorithmException {
        MessageDigest md;
        md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[]digest = md.digest();
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b: digest){
            stringBuilder.append(String.format("%02X",b));
        }
        return stringBuilder.toString();
    }
}
