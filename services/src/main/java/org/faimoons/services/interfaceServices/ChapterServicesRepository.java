package org.faimoons.services.interfaceServices;

import org.faimoons.model.ChapterBooks;

import java.util.List;
import java.util.Optional;

public interface ChapterServicesRepository{


    ChapterBooks findByNumberChapterAndIdBook(int numberChapter, Long idBook);


    List<Integer> getAllNumberChapterForBook(Long idBook);


    <S extends ChapterBooks> S save(S s);


    <S extends ChapterBooks> Iterable<S> saveAll(Iterable<S> iterable);


    Optional<ChapterBooks> findById(Long aLong);


    boolean existsById(Long aLong);


    Iterable<ChapterBooks> findAll();


    Iterable<ChapterBooks> findAllById(Iterable<Long> iterable);

    long count();


    void deleteById(Long aLong);


    void delete(ChapterBooks chapterBooks);


    void deleteAll(Iterable<? extends ChapterBooks> iterable);


    void deleteAll();
}
