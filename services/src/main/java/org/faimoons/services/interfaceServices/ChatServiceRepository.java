package org.faimoons.services.interfaceServices;

import org.faimoons.model.Chat;
import org.faimoons.repository.ChatRepository;

import java.util.List;
import java.util.Optional;

public interface ChatServiceRepository  {

    List<Chat> findAllByIdBookOrderByTimeMessageDesc(Long idBook);


    <S extends Chat> S save(S s);


    <S extends Chat> Iterable<S> saveAll(Iterable<S> iterable);


    Optional<Chat> findById(Long aLong);


    boolean existsById(Long aLong);


    Iterable<Chat> findAll();


    Iterable<Chat> findAllById(Iterable<Long> iterable);


    long count();


    void deleteById(Long aLong);


    void delete(Chat chat);


    void deleteAll(Iterable<? extends Chat> iterable);


    void deleteAll();
}
