package org.faimoons.services.interfaceServices;

import org.faimoons.model.DescriptionBooks;
import org.faimoons.repository.DescriptionBooksRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface DescriptionBooksServicesRepository {

    Iterable<DescriptionBooks> findAll(Sort sort);


    Page<DescriptionBooks> findAll(Pageable pageable);

    Collection<DescriptionBooks> findAllByCreateDateBetween();


    Collection<DescriptionBooks> findAllByNameBookContaining(String nameBook);


    Long countByNameBookContaining(String nameBook);


    Long countAllBy();



    <S extends DescriptionBooks> S save(S s);


    <S extends DescriptionBooks> Iterable<S> saveAll(Iterable<S> iterable);


    Optional<DescriptionBooks> findById(Long aLong);


    boolean existsById(Long aLong);


    List<DescriptionBooks> findAll();


    Iterable<DescriptionBooks> findAllById(Iterable<Long> iterable);


    long count();


    void deleteById(Long aLong);


    void delete(DescriptionBooks descriptionBooks);


    void deleteAll(Iterable<? extends DescriptionBooks> iterable);


    void deleteAll();
}
