package org.faimoons.services.interfaceServices;

import org.faimoons.model.Books;
import org.faimoons.repository.BooksRepository;

import java.util.Optional;

public interface BookServicesRepository  {

    <S extends Books> S save(S s);


    <S extends Books> Iterable<S> saveAll(Iterable<S> iterable);


    Optional<Books> findById(Long aLong);


    boolean existsById(Long aLong);


    Iterable<Books> findAll();


    Iterable<Books> findAllById(Iterable<Long> iterable);


    long count();


    void deleteById(Long aLong);


    void delete(Books books);



    void deleteAll();
}
