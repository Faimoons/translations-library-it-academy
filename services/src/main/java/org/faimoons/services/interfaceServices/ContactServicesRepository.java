package org.faimoons.services.interfaceServices;

import org.faimoons.model.Contact;
import org.faimoons.repository.ContactRepository;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;

public interface ContactServicesRepository {


    Contact findByLogin(String login);


    <S extends Contact> S save(S s) throws NoSuchAlgorithmException;


    Optional<Contact> findById(Long aLong);


    Iterable<Contact> findAll();


    Iterable<Contact> findAllById(Iterable<Long> iterable);


    long count();


    void deleteById(Long aLong);


    void delete(Contact contact);
}
